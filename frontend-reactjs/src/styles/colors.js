const COLORS = {
  black: "#000000",
  beije: "#ebe9d7",
  doveGray: "#707070",
  doveGrayLight: "#9e9e9e",

  frenchRose: "#ee4c77",
  fountainBlue: "#57bbbc",
  tuna: "#383743",
  white: "#ffffff",
};

export default {
  ...COLORS,
};
