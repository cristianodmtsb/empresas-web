import { createGlobalStyle } from "styled-components";
import colors from "./colors";

import Black from "../assets/fonts/Roboto-Black.ttf";
import Bold from "../assets/fonts/Roboto-Bold.ttf";
import Light from "../assets/fonts/Roboto-Light.ttf";
import Medium from "../assets/fonts/Roboto-Medium.ttf";
import Regular from "../assets/fonts/Roboto-Regular.ttf";

export default createGlobalStyle`

  @font-face {
    font-family: 'Black';
    src: url(${Black});
  }
  @font-face {
    font-family: 'Bold';
    src: url(${Bold});
  }
  @font-face {
    font-family: 'Light';
    src: url(${Light});
  }

  @font-face {
    font-family: 'Medium';
    src: url(${Medium});
  }

  @font-face {
    font-family: 'Regular';
    src: url(${Regular});
  }

  /* Change autocomplete styles in WebKit */
  input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  textarea:-webkit-autofill,
  textarea:-webkit-autofill:hover,
  textarea:-webkit-autofill:focus,
  select:-webkit-autofill,
  select:-webkit-autofill:hover,
  select:-webkit-autofill:focus {
    -webkit-box-shadow: 0 0 0px 1000px $white inset;
    box-shadow: 0 0 0px 1000px $white inset;
  }

  *,
  *::after,
  *::before {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }


  html {
    /* This defines what 1rem is.  */
    font-size: 62.5% !important; /*1 rem = 10px; 10px/16px = 62.5% */
    /* Use 10px as base is good to make calculation faster (ex: 176px = 1.76rem) */
  }

  body {
    box-sizing: border-box;
    overflow-x: hidden;
  }

  /* Typography */

  body {
    font-family: 'Regular', sans-serif !important;
    font-weight: 400;
    font-size: 1.4rem;
    line-height: 1.3;
    color: ${colors.tuna};
  }

  html,
  body,
  #root {
    height: 100%;
  }

  input,
  select,
  button {
    /* to void the iOS auto zoom, the inputs font-sies are set as 16px.  */
    font-size: 1.6rem;
  }

  ul {
    list-style: none;
  }

  button{
    cursor: pointer;
  }

  a {
    text-decoration: none;
  }

  sub {
    font-size: 10px;
  }

  img { max-width: 100%; height: auto;}
  a, input, button {
    transition: all 0.7s;
  }

`;
