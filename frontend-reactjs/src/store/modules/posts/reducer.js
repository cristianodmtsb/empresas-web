import produce from "immer";

export default function posts(state = [], action) {
  switch (action.type) {
    case "@company/SUCCESS":
      return [...state, ...action.posts];
    case "@company/ADD":
      return [...state, action.post];

    case "@company/DELETE":
      return produce(state, (draft) => {
        const postIndex = draft.findIndex((p) => p.id === action.id);

        if (postIndex >= 0) {
          draft.splice(postIndex, 1);
        }
      });
    default:
      return state;
  }
}
