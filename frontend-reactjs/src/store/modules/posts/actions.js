export function listPostRequest() {
  return {
    type: "@company/REQUEST",
  };
}
export function listPostSuccess(posts) {
  return {
    type: "@company/SUCCESS",
    posts,
  };
}

export function addPost(post) {
  return {
    type: "@company/ADD",
    post: {
      id: Math.random() * 100,
      title: post.title,
      content: post.content,
      category: post.category,
    },
  };
}

export function deletePost(id) {
  return { type: "@company/DELETE", id };
}
