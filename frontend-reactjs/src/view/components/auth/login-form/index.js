import React from "react";
import { useForm } from "react-hook-form";
import { isEmail } from "../../utils/isEmail";
import Loading from "../../Loading";

import * as S from "./styles";

const LoginForm = () => {
  const { register, handleSubmit, errors, formState } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <S.InputEmail
          name="email"
          ref={register({
            required: true,
            validate: (input) => isEmail(input),
          })}
          style={{ borderColor: errors.email && "red" }}
          placeholder="E-mail"
        />
        <S.InputPassword
          name="password"
          ref={register({
            required: true,
            minLength: 6,
          })}
          style={{ borderColor: errors.password && "red" }}
          placeholder="Senha"
          type="password"
        />

        {(errors.email || errors.password) && (
          <S.ErrorsMessage>
            Credenciais informadas são inválidas, tente novamente.
          </S.ErrorsMessage>
        )}
        <S.Button
          type="submit"
          disabled={formState.isSubmitting || errors.email || errors.password}
          style={{
            backgroundColor: (errors.email || errors.password) && "#748383",
          }}
        >
          ENTRAR
        </S.Button>

        {formState.isSubmitting && <Loading />}
      </form>
    </>
  );
};

export default LoginForm;
