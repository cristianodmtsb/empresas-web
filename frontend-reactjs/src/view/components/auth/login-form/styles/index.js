import styled from "styled-components";
import icEmail from "../img/ic-email.png";
import icPassword from "../img/ic-password.png";

export const Input = styled.input`
  background-color: transparent;
  background-position: center left;
  background-repeat: no-repeat;
  border: 0;
  border-bottom: 1px solid #383743;
  display: block;
  font-size: 1.4rem;
  padding: 0.7rem 1.2rem 0.7rem 2.7rem;
  margin-bottom: 1.8rem;
  width: 100%;
  &:focus {
    border-bottom: 1px solid #000000;
    outline: none;
  }
`;

export const InputEmail = styled(Input)`
  background-image: url(${icEmail});
`;
export const InputPassword = styled(Input)`
  background-image: url(${icPassword});
`;

export const Button = styled.button`
  background-color: #57bbbc;
  border: 0;
  border-radius: 0.4rem;
  color: #ffffff;
  display: block;
  font-size: 1.4rem;
  font-family: "Bold";
  margin: 3rem 5%;
  padding: 1.45rem 0;
  width: 90%;
  &:hover {
    background-color: #57abac;
    cursor: pointer;
  }
`;

export const ErrorsMessage = styled.div`
  color: #ff0f44;
  font-size: 0.8rem;
  text-align: center;
`;
