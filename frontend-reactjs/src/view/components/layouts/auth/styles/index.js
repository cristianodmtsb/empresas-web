import styled from "styled-components";

export const AuthLayout = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #ebe9d7;
  height: 100%;
`;

export const Content = styled.div`
  width: 100%;
  max-width: 25rem;
`;

export const Logo = styled.img`
  padding: 0.9rem 0.9rem 2.5rem 0.9rem;
`;
