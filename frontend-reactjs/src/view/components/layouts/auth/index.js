import React from "react";

import logoSmall from "./img/logo-home.png";
import logoMedium from "./img/logo-home@2x.png";
import logoLarge from "./img/logo-home@3x.png";

import * as S from "./styles";

const AuthLayout = ({ children }) => {
  return (
    <S.AuthLayout>
      <S.Content>
        <S.Logo
          src={logoSmall}
          alt="Ioasys Business"
          srcSet={`${logoSmall} 300w, ${logoMedium} 768w, ${logoLarge} 1280w`}
        />
        {children}
      </S.Content>
    </S.AuthLayout>
  );
};

export default AuthLayout;
