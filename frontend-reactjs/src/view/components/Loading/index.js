import React from "react";
import Spinner from "../Spinner";

import * as S from "./styles";

const Loading = () => {
  return (
    <S.Loading>
      <Spinner size={9.5} />
    </S.Loading>
  );
};

export default Loading;
