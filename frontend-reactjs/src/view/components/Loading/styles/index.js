import styled from "styled-components";

export const Loading = styled.div`
  position: fixed;
  display: flex;
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.6);
  top: 0;
  left: 0;
  justify-content: center;
  align-items: center;
`;
