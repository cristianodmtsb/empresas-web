import React from "react";
import PropTypes from "prop-types";

import * as S from "./styles";

const Spinner = (props) => {
  const { block, mt, mb, secondary, size } = props;
  return (
    <S.Block block={block}>
      <S.Spinner size={size} mt={mt} mb={mb}>
        <S.SVG viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
          <S.CircleSvg secondary={secondary} cx="50" cy="50" r="40" />
        </S.SVG>
      </S.Spinner>
    </S.Block>
  );
};

Spinner.propTypes = {
  block: PropTypes.bool,
  mt: PropTypes.number,
  mb: PropTypes.number,
  secondary: PropTypes.bool,
  size: PropTypes.any,
};

export default Spinner;
