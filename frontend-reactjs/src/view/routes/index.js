import React from "react";
import { Switch } from "react-router-dom";
import Route from "./Routes";

// import Dashboard from "../pages/Dashboard";

import { LoginPage } from "../pages";
// import Register from "../pages/Register";

export default function Routes() {
  return (
    <Switch>
      {/* <Route path="/dashboard" exact component={Dashboard} isPrivate /> */}

      <Route exact path="/" component={LoginPage} />
      {/* <Route path="/register" component={Register} /> */}
    </Switch>
  );
}
