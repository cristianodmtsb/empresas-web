import styled from "styled-components";

export const TitleLogin = styled.h3`
  font-family: "Bold";
  font-size: 1.6rem;
  padding: 1rem 2rem;
  text-align: center;
`;

export const TextLogin = styled.p`
  font-size: 1rem;
  text-align: center;
  padding: 1rem 1.5rem 2rem;
`;
