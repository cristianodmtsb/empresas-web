import React from "react";

import { LoginForm } from "../../components/auth";

import * as S from "./styles";

const LoginPage = () => {
  return (
    <>
      <S.TitleLogin>BEM-VINDO AO EMPRESAS</S.TitleLogin>
      <S.TextLogin>
        Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
      </S.TextLogin>

      <LoginForm />
    </>
  );
};

export default LoginPage;
